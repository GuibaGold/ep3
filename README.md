# House Cleaning

## Autores

| Nome  | Matrícula  | GitLab |
|---|---|---|
| Bruno Carmo Nunes  | 18/0117548  | [brunocmo](https://github.com/brunocmo) |
| Guilherme Botelho Dourado	  | 16/0123020 | [GuibaGold](https://github.com/GuibaGold) |

## Sobre

Este é um aplicação web que faz a requisição de uma prestadora de serviço domestica.
Onde o usuário é criado e pode requisitar uma diarista.

## Instalação

Tenha o Ruby on Rails na versão 2.5.1

> * ``` git clone https://gitlab.com/GuibaGold/ep3.git ```

## Execução

> * Entre no terminal e digite ```cd house_cleaning ```.
> * Verifique o bundle com ``` bundle install ```
> * Faça as migrações com ``` rails db:migrate ```
> * E rode o servidor com ``` rails s ```
> * Depois entre no [Site](localhost:3000)

## Funcionalidades

* Criação do usuário
* Requisição de diaristas por usuário

## UML

Se encontra no diretorio house_cleaning/doc

## Gems utilizadas

rails

sqlite3

puma

sass-rails

webpacker

turbolinks

jbuilder

bootsnap

tzinfo-data

devise

bootstrap-sass

fonts-awesome-rails

sassc-rails

railroady

jquery-rails

byebug

web-console

listen

pring

spring-watcher-listen

capybara

selenium-webdriver

webdrivers
