Rails.application.routes.draw do
  resources :pedidos
  devise_for :users
  resources :cleaners

  root to: "pedidos#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
