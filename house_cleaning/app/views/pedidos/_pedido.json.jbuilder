json.extract! pedido, :id, :nome, :local, :numero_dias, :exigencias, :preco, :created_at, :updated_at
json.url pedido_url(pedido, format: :json)
