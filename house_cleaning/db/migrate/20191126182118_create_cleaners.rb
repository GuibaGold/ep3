class CreateCleaners < ActiveRecord::Migration[6.0]
  def change
    create_table :cleaners do |t|
      t.string :nome
      t.integer :idade
      t.string :servicos
      t.integer :dias_disponiveis
      
      t.timestamps
    end
    add_index :cleaners, :nome,             :unique => true
  end
end
