class AddUserToCleaner < ActiveRecord::Migration[6.0]
  def change
    add_column :cleaners, :user_id, :integer
    add_index :cleaners, :user_id
  end
end
