class CreatePedidos < ActiveRecord::Migration[6.0]
  def change
    create_table :pedidos do |t|
      t.string :nome
      t.string :local
      t.integer :numero_dias
      t.string :exigencias
      t.float :preco

      t.timestamps
    end
  end
end
